# fake news detector #

Blocks websites that publish fake news:

![Screenshot of fake news detector ](/img/ss_img.png?raw=true)

Hides their posts on Facebook:

![Video of fake news detector](/img/ss_video.gif?raw=true)

Also includes a [clustering analysis](discriminating-fake-news-from-real-news.ipynb)
that could lead to an algorithm to automatically detect Fake News.

## Manifesto ##

	It aims to stop these websites:

> [...] fake news = spam sites built from scripts consisting largely of a
> backcatalog nonsense stories (example- "Pope endorses Putin for US Presidential
> Election") with a one or two carefully produced fake stories as a "payload",
> instead of a purchasing call-to-action.

## Created by
Sanchit Nanda 
Shubhra Duggal

